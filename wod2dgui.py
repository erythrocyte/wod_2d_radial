import sys

import os
a = os.getcwd()

sys.path.insert(0, a + '/gui/')
sys.path.insert(0, a + '/gui/mainFrame/')
sys.path.insert(0, a + '/src/')
sys.path.insert(0, a + '/src/initFileWork')

from PyQt5.QtWidgets import QApplication

import mainFrame
from mainFrame import MainFrame1DWOD

def main():
	app = QApplication(sys.argv)
	mainFrame = MainFrame1DWOD()
	mainFrame.showMaximized()
	sys.exit(app.exec_())
	
if __name__ == '__main__':
	main()

