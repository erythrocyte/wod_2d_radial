import node
from node import Node

class Cell:
    def __init__(self):
        self.area = 0.0
        self.faces = []
        self.nodes = []
        self.cntr = Node(0.0, 0.0)
