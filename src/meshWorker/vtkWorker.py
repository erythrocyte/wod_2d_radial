class VTKWorker:
    def __init__(self, grid, fn):
        self.fn = fn
        self.grid = grid

    def SaveVTK(self):
        f = open(self.fn, 'w')
        self.SaveMainHeader(f)
        self.SaveCaption(f, 'mesh')
        self.SaveDataset(f)
        self.SavePoints(f)
        self.SaveCells(f)
        self.SaveCellData(f, 0, 'index')        
        f.close()

    def SaveMainHeader(self, f):
        f.write('# vtk DataFile Version 3.0\n')

    def SaveCaption(self, f, capt):
        f.write(capt + '\n')

    def SaveDataset(self, f):
        f.write ('ASCII\n')
        f.write ('DATASET UNSTRUCTURED_GRID\n')

    def SavePoints(self, f):
        f.write('POINTS {0} float\n'.format(len(self.grid.nodes)))
        for nd in self.grid.nodes:
            f.write('{0} {1} {2}\n'.format(nd.x, nd.y, 0.0))

    def GetCellPointCount(self):
        result = 0
        for c in self.grid.cells:
            result += (len(c.nodes) + 1)
            # print ('ind = {0} : nodes = {1}, sum_nodes = {2}'.format(c.ind, len(c.nodes), result))

        return result


    def SaveCells(self, f):
        self.SaveCellsPoints(f)
        self.SaveCellsTypes(f)

    def SaveCellsPoints(self, f):
        f.write('CELLS {0} {1}\n'.format(len(self.grid.cells), self.GetCellPointCount()))

        for c in self.grid.cells:
            s = str(len(c.nodes)) + '\t'
            for ni in c.nodes:
                s += str(ni) + ' '
                
            s = str(s).strip()
            f.write(s + '\n')

    def SaveCellsTypes(self, f):
        f.write ('CELL_TYPES {0}\n'.format(len(self.grid.cells)))
        for c in self.grid.cells:
            f.write('9\n')

    def SaveCellData(self, f, dataType, capt):
        f.write('CELL_DATA {0}\n'.format(len(self.grid.cells)))
        f.write('SCALARS {0} float 1\n'.format(capt))
        f.write('LOOKUP_TABLE default\n')
        if (dataType == 0):
            self.SaveCellDataIndex(f)
            
    def SaveCellDataIndex(self, f):
        for c in self.grid.cells:
            f.write('{0}\n'.format(c.ind))



